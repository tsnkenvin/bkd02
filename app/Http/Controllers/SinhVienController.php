<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Lop;
use App\Models\SinhVien;

class SinhVienController extends Controller
{
    protected $table = 'sinh_vien';
    public function view_all()
    {
    	$array_sinh_vien = SinhVien::with('lop')->get();

    	return view("$this->table.view_all",[
    		"array_sinh_vien" => $array_sinh_vien
    	]);
    }
    public function view_insert()
    {
        $array_lop = Lop::get();
    	return view("$this->table.view_insert",[
            'array_lop' => $array_lop
        ]);
    }
    public function process_insert(Request $rq)
    {
    	SinhVien::create($rq->all());

    	return redirect()->route("$this->table.view_all");
    }
    public function view_update($ma)
    {
    	$sinh_vien = SinhVien::find($ma);
        $array_lop = Lop::get();

    	return view("$this->table.view_update",[
    		"sinh_vien" => $sinh_vien,
            "array_lop" => $array_lop,
    	]);
    }
    public function process_update($ma, Request $rq)
    {
    	SinhVien::find($ma)->update($rq->all());
    }
    public function delete($ma)
    {
    	SinhVien::find($ma)->delete();
    }

}
