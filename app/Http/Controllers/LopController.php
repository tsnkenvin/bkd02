<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Lop;

class LopController extends Controller
{
    public function view_all()
    {
    	$array_lop = Lop::all();

    	return view("lop.view_all",[
    		'array_lop' => $array_lop
    	]);
    }
    public function view_insert()
    {
    	return view("lop.view_insert");
    }
    public function process_insert(Request $rq)
    {
    	// $lop = new Lop();
    	// $lop->ten = $rq->ten;
    	// $lop->save();

    	Lop::create($rq->all());

    	return redirect()->route('lop.view_all');
    }
    public function view_update($ma)
    {
    	// $lop = Lop::where('ma','=',$ma)->first();

    	$lop = Lop::find($ma);

    	return view("lop.view_update",[
    		'lop' => $lop
    	]);
    }
    public function process_update($ma, Request $rq)
    {
    	// $lop = Lop::find($ma);
    	// $lop->ten = $rq->ten;
    	// $lop->save();

    	// Lop::find($ma)->update([
    	// 	'ten' => $rq->ten
    	// ]);

    	Lop::find($ma)->update($rq->all());
    }
    public function delete($ma)
    {
    	// $lop = Lop::find($ma);
    	// $lop->delete();

    	Lop::find($ma)->delete();
    }
    public function view_array_sinh_vien_by_lop($ma)
    {
        $array_sinh_vien = Lop::find($ma)->array_sinh_vien;
        return $array_sinh_vien;
    }

}
