<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lop';
    public $timestamps = false;
    protected $fillable = ['ten'];
    protected $primaryKey = 'ma';
    public function array_sinh_vien()
    {
        return $this->hasMany('App\Models\SinhVien','ma_lop','ma');
    }
}
