<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    protected $table = 'sinh_vien';
    public $timestamps = false;
    protected $fillable = [
    	'ho',
    	'ten',
    	'ngay_sinh',
    	'gioi_tinh',
    	'ma_lop',
    ];
    protected $primaryKey = 'ma';
    public function lop()
    {
        return $this->belongsTo('App\Models\Lop','ma_lop');
    }
}
