<a href="{{ route('lop.view_insert') }}">
	Thêm
</a>
<table border="1" width="100%">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Xem sinh viên lớp này</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	@foreach ($array_lop as $lop)
		<tr>
			<td>
				{{ $lop->ma }}
			</td>
			<td>
				{{ $lop->ten }}
			</td>
			<td>
				<a href="{{ route('lop.view_array_sinh_vien_by_lop',['ma' => $lop->ma]) }}">
					Xem sinh viên
				</a>
			</td>
			<td>
				<a href="{{ route('lop.view_update',['ma' => $lop->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('lop.delete',['ma' => $lop->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>