<form action="{{ route('sinh_vien.process_insert') }}" method="post">
	{{ csrf_field() }}
	Họ
	<input type="text" name="ho">
	<br>
	Tên
	<input type="text" name="ten">
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh">
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1">Nữ
	<input type="radio" name="gioi_tinh" value="0">Nam
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Thêm</button>
</form>