<form action="{{ route('sinh_vien.process_update',['ma' => $sinh_vien->ma]) }}" method="post">
	{{ csrf_field() }}
	Họ
	<input type="text" name="ho" value="{{ $sinh_vien->ho }}">
	<br>
	Tên
	<input type="text" name="ten" value="{{ $sinh_vien->ten }}">
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh" value="{{ $sinh_vien->ngay_sinh }}">
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1" 
	@if ($sinh_vien->gioi_tinh==1)
		checked 
	@endif>Nữ
	<input type="radio" name="gioi_tinh" value="0" 
	@if ($sinh_vien->gioi_tinh==0)
		checked 
	@endif>Nam
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}"
			@if ($lop->ma == $sinh_vien->ma_lop)
				selected 
			@endif
			>
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Sửa</button>
</form>