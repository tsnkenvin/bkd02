<?php

$controller = "LopController";
Route::group(["prefix" => "lop","as" => "lop."],function() use ($controller){
	Route::get("","$controller@view_all")->name("view_all");
	Route::get("view_insert","$controller@view_insert")->name("view_insert");
	Route::post("process_insert","$controller@process_insert")->name("process_insert");
	Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
	Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
	Route::get("delete/{ma}","$controller@delete")->name("delete");
	
	Route::get("view_array_sinh_vien_by_lop/{ma}","$controller@view_array_sinh_vien_by_lop")->name("view_array_sinh_vien_by_lop");
});

$controller = "SinhVienController";
Route::group(["prefix" => "sinh_vien","as" => "sinh_vien."],function() use ($controller){
	Route::get("","$controller@view_all")->name("view_all");
	Route::get("view_insert","$controller@view_insert")->name("view_insert");
	Route::post("process_insert","$controller@process_insert")->name("process_insert");
	Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
	Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
	Route::get("delete/{ma}","$controller@delete")->name("delete");
});

